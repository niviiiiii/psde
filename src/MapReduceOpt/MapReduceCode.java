package MapReduceOpt;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;



public class MapReduceCode extends Configured implements Tool{

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new MapReduceCode(), args);
		System.exit(exitCode);
	}
	public int run(String[] args) throws Exception {
		Scan scan  = getScanObject();		
		Configuration config = HBaseConfiguration.create();
	
		Job job = new Job(config,"ProductDataExtracter");
		job.setJarByClass(MapReduceCode.class);
		job.setJobName("ProductDataExtracter");
		TableMapReduceUtil.initTableMapperJob(
			    "Products",        // input table
				scan,               // Scan instance to control CF and attribute selection
				Mapper.class,     // mapper class
				ImmutableBytesWritable.class,
	            IntWritable.class,
	            job);
		
        job.setOutputFormatClass(NullOutputFormat.class);
        job.setNumReduceTasks(0);
        
        int returnValue = job.waitForCompletion(true) ? 0:1;
         
        if(job.isSuccessful()) {
            System.out.println("Job was successful");
        } else if(!job.isSuccessful()) {
            System.out.println("Job was not successful");           
        }
         
        return returnValue;
	}
	
	/**
	 * Scan object for hadoop table scan
	 * @return scan
	 */
	public static Scan getScanObject() {
		Scan scan = new Scan();
		scan.setCaching(500);        // 1 is the default in Scan, which will be bad for MapReduce jobs
		scan.setCacheBlocks(false); 	
		return scan;
	}

}

class Mapper extends TableMapper<ImmutableBytesWritable, IntWritable> {
	
    protected void map(ImmutableBytesWritable key, Result value, Context context) throws java.io.IOException, InterruptedException {
        System.out.println(key.toString()+","+ Bytes.toString(key.get()));
        System.out.println("**************************************************************************************");
        List<Cell> keyValueList = value.listCells();
        System.out.println(keyValueList.toString());
        System.out.println("**************************************************************************************");
        System.out.println(context.toString());
        System.out.println("**************************************************************************************");
        String tableName = "Products";
        String filePath = "D:/fileformat.csv";
        File csvOutputFile = new File(filePath);
        String family;
        String qualifier;
        Configuration config = HBaseConfiguration.create();
        config.clear();
                 /**
                  config.set("hbase.zookeeper.quorum","sfdllmn001.gid.gap.com");
                  config.set("hbase.zookeeper.property.clientPort","2181");
                  config.set("hbase.master", "10.116.124.146:2181");
                    HBaseAdmin.checkHBaseAvailable(config);
          log.info "Connected to Remote Hbase";
        */
        HBaseAdmin admin = new HBaseAdmin(config);
        HTableDescriptor desc = admin.getTableDescriptor(tableName.getBytes());
        OutputStream os = new FileOutputStream(filePath);
        CsvOutputStream cos = new CsvOutputStream(os);
        HTable table = new HTable(config, tableName);
        Scan scan = new Scan();
        //scan.setFilter(new PageFilter(25));
               ResultScanner resultScanner = table.getScanner(scan);
             //  Result result;
               
             
               
               List<HBaseCol> columns = new ArrayList<HBaseCol>();
               List<String> values = new ArrayList<String>();
               List<String> rows = new ArrayList<String>();
        while((value=resultScanner.next()) != null) {
            
            String row=Bytes.toString(value.getRow());
            rows.add(row);
            values.clear();
                for(KeyValue key1: value.list()) {
                
                    family = new String(key1.getFamily());
                    qualifier = new String(key1.getQualifier());
                    columns.add(new HBaseCol(family,qualifier));
                    byte[] val = value.getValue(Bytes.toBytes(family), Bytes.toBytes(qualifier));
                    values.add(row+":"+family+":"+qualifier+":"+Bytes.toString(val));
                }
           System.out.println("Writing to CSV");
                cos.writeLine(values.toArray(new String[0]));
                values.clear();
        }
        cos.close();
        }
}
class HBaseCol {
public String family;
public String qualifier;
public HBaseCol(String family,String qualifier){
this.family = family;
this.qualifier = qualifier;
}
}
class CsvOutputStream implements Closeable{
OutputStream out;
BufferedWriter bufferedWriter;
String delimiter=",";
public CsvOutputStream(OutputStream out) {
this.out = out;
}
public void init() throws UnsupportedEncodingException {
OutputStreamWriter streamWriter = new OutputStreamWriter(out);
bufferedWriter = new BufferedWriter(streamWriter);
}
public static String dumpCsvLine(String []vals, String delimiter) {
StringBuffer s = new StringBuffer();
for(int i=0; i<vals.length; i++) {
if (i>0) {
s.append(delimiter);
}
s.append(dumpCsvValue(vals[i],delimiter));
}
return s.toString();
}
public static String dumpCsvValue(String value, String delimiter) {
if (value.indexOf(delimiter)>=0 || value.contains("\"") || value.contains("\n") || value.contains("\r")) {
return "\""+value.replace(" \" ", " \"\" ")+"\"";

} else {
return value;
}
}
public void writeLine(String[]vals) throws IOException {
if (bufferedWriter == null) {
init();
}
bufferedWriter.write(dumpCsvLine(vals,delimiter)+"\n");
}
public void close() throws IOException {
bufferedWriter.close();
}
}